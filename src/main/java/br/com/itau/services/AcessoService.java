package br.com.itau.services;

import br.com.itau.clients.ClienteClient;
import br.com.itau.exceptions.ClienteNotFoundException;
import br.com.itau.models.Acesso;
import br.com.itau.producer.LogAcesso;
import br.com.itau.producer.LogAcessoProducerKafka;
import br.com.itau.repositories.AcessoRepository;
import br.com.itau.clients.SalaClient;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Random;

@Service
public class AcessoService {

    @Autowired
    AcessoRepository acessoRepository;

    @Autowired
    ClienteClient clienteClient;

    @Autowired
    LogAcessoProducerKafka logAcessoProducerKafka;

    @Autowired
    SalaClient salaClient;


    public Acesso salvarAcesso(Acesso acesso){

        validarCliente(acesso.getIdCliente());
        return acessoRepository.save(acesso);
    }

    public void validarCliente(int idCliente){

        try {
            System.out.println("**** Acesso: Tentou validar cliente " + LocalDateTime.now() + " idCliente: " + idCliente);
            clienteClient.getClienteById(idCliente);
        }catch (FeignException.BadRequest e){
            throw new ClienteNotFoundException();
        }
    }

    public void validarSala(int idSala){

            System.out.println(">>>> Acesso: Tentou validar Sala " + LocalDateTime.now() + " idSala: " + idSala);
            salaClient.getSalaById(idSala);
            // PAREI Aqui

    }

    public boolean buscarAcessoPorId(int idSala, int idCliente){
        List<Acesso> acessoList = acessoRepository.findByIdSalaAndIdCliente(idSala, idCliente);

        Random acessoRandomico = new Random();
        int numeroEscolhido = acessoRandomico.nextInt(2);

        LogAcesso logAcesso = new LogAcesso();
        logAcesso.setIdCliente(idCliente);
        logAcesso.setIdSala(idSala);


        if (numeroEscolhido == 1){
            logAcesso.setTemAcesso(true);
        }else{
            logAcesso.setTemAcesso(false);
        }

        logAcessoProducerKafka.enviarAoKafka(logAcesso);

        return logAcesso.isTemAcesso();

    }
}

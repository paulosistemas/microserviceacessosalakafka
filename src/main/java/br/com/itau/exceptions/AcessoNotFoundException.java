package br.com.itau.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Usuário não possui acesso a sala informada.")
public class AcessoNotFoundException extends RuntimeException{
}

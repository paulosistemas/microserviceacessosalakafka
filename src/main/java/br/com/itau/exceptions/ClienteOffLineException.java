package br.com.itau.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_GATEWAY, reason = "O sistema de Clientes está Offline")
public class ClienteOffLineException extends RuntimeException  {
}

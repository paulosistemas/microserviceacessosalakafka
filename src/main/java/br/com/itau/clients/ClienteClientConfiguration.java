package br.com.itau.clients;

import feign.Feign;
import feign.RetryableException;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class ClienteClientConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder(){
        return new ClienteClientDecoder();
    }

    @Bean
    public Feign.Builder builder(){

        // o Decoder já tratou o erro quando chega no fallback
        FeignDecorators feignDecorators = FeignDecorators.builder()
                .withFallback(new ClienteClientFallBack(), RetryableException.class)
                .build();

        return Resilience4jFeign.builder(feignDecorators);
    }
}

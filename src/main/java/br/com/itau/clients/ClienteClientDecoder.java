package br.com.itau.clients;

import br.com.itau.exceptions.AcessoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;

public class ClienteClientDecoder implements ErrorDecoder{

    private ErrorDecoder errorDecoder = new ErrorDecoder.Default();

    @Override
    public Exception decode(String s, Response response){
        if (response.status() == 404){
            throw new AcessoNotFoundException();

        }else{
            return errorDecoder.decode(s, response);
        }
    }
}

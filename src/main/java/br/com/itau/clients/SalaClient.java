package br.com.itau.clients;

import br.com.itau.models.Sala;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "sala")
public interface SalaClient {

    @GetMapping("/v1/sala/{idSala}")
    Sala getSalaById(@PathVariable int idSala);
}

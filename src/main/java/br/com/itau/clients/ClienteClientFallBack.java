package br.com.itau.clients;

import br.com.itau.exceptions.ClienteOffLineException;
import br.com.itau.models.Cliente;

public class ClienteClientFallBack implements ClienteClient {

    @Override
    public Cliente getClienteById (int idCliente){

        throw new ClienteOffLineException();

    }
}

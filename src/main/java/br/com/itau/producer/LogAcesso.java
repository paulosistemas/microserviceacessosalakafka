package br.com.itau.producer;

public class LogAcesso {

    private int idCliente;
    private String separa1;
    private int idSala;
    private String separa2;
    private boolean temAcesso;
    private String separa3;

    public LogAcesso() {
        separa1 = ";";
        separa2 = ";";
        separa3 = ";";
    }

    public int getIdCliente() {
        return idCliente;
    }

    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    public String getSepara1() {
        return separa1;
    }

    public void setSepara1(String separa1) {
        this.separa1 = separa1;
    }

    public int getIdSala() {
        return idSala;
    }

    public void setIdSala(int idSala) {
        this.idSala = idSala;
    }

    public String getSepara2() {
        return separa2;
    }

    public void setSepara2(String separa2) {
        this.separa2 = separa2;
    }

    public boolean isTemAcesso() {
        return temAcesso;
    }

    public void setTemAcesso(boolean temAcesso) {
        this.temAcesso = temAcesso;
    }

    public String getSepara3() {
        return separa3;
    }

    public void setSepara3(String separa3) {
        this.separa3 = separa3;
    }
}

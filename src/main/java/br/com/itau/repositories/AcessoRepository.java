package br.com.itau.repositories;

import br.com.itau.models.Acesso;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface AcessoRepository extends CrudRepository <Acesso, Integer>{

    List<Acesso> findByIdSalaAndIdCliente(int idSala, int idCliente);
}

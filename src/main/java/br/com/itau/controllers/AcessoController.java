package br.com.itau.controllers;

import br.com.itau.models.Acesso;
import br.com.itau.services.AcessoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoService acessoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Acesso salvarAcesso(@RequestBody @Valid Acesso acesso){
        return acessoService.salvarAcesso(acesso);
    }

    @GetMapping("/{idCliente}/{idSala}")
    @ResponseStatus(HttpStatus.OK)
    public boolean buscarAcesso(@PathVariable(name="idCliente") int idCliente, @PathVariable(name="idSala") int idSala){
        try {

            return acessoService.buscarAcessoPorId(idCliente, idSala);

        }catch(RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

}
